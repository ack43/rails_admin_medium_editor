require 'rails_admin/config/fields/types/text'

module RailsAdmin
  module Config
    module Fields
      module Types
        class MediumEditor < RailsAdmin::Config::Fields::Types::Text
          # Register field type for the type loader
          RailsAdmin::Config::Fields::Types.register(self)

          # If you want to have a different toolbar configuration for wysihtml5
          # you can use a Ruby hash to configure these options:
          # https://github.com/jhollingworth/bootstrap-wysihtml5/#advanced
          register_instance_option :config_options do
            nil
          end

          register_instance_option :css_location do
            ActionController::Base.helpers.asset_path('medium-editor/medium-editor.css')
          end

          register_instance_option :js_location do
            ActionController::Base.helpers.asset_path('medium-editor.js')
          end

          register_instance_option :partial do
            :form_medium_editor
          end

          
        end
      end
    end
  end
end
