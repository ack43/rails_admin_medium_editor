module RailsAdminMediumEditor
  class Engine < ::Rails::Engine
    # isolate_namespace Hancock::Pages

    initializer "RailsAdminMediumEditor.assets" do |app|
      app.config.assets.precompile += %w( rails_admin/medium-editor.* medium-editor medium-editor/* )      
    end

  end
end
